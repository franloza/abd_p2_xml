package es.ucm.abd.practica2.dao;

import es.ucm.abd.practica2.model.Abedemon;
import org.w3c.dom.Element;

import javax.xml.namespace.QName;
import javax.xml.xquery.*;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementación concreta del DAO que hace llamadas a eXist.
 * 
 * @author Manuel Montenegro (mmontene@ucm.es)
 */
public class AbedemonDAOImpl implements AbedemonDAO {

    private final XQDataSource ds;

    public AbedemonDAOImpl(XQDataSource ds) {
        this.ds = ds;
    }

    
    /**
     * Obtiene los tipos de especies disponibles en la BD.
     * 
     * @return Lista de tipos de especies (sin duplicados)
     */
    @Override
    public List<String> getTypes() {
        InputStream is = getClass().getResourceAsStream("Abedemon1.xquery");
        XQConnection con;
        List<String> types = new ArrayList<>();
        try {
            con = ds.getConnection();
            XQPreparedExpression exp = con.prepareExpression(is);
            XQResultSequence rs = exp.executeQuery();
            while (rs.next()) {
                types.add(rs.getItemAsString(null));
            }
            rs.close();
            exp.close();
            con.close();
        } catch (XQException e) {
            e.printStackTrace();
        }
        return types;
    }

    /**
     * Obtiene las especies de abedemon de un determinado.
     * 
     * @param type Tipo a buscar
     * @return Especies encontradas del tipo pasado como parámetro.
     */
    @Override
    public List<Abedemon> getAbedemonsOf(String type) {
        InputStream is = getClass().getResourceAsStream("Abedemon2.xquery");
        XQConnection con;
        List<Abedemon> abedemons = new ArrayList<>();
        String id, nombre;
        int numAtaques;
        Element elem;

        try {
            con = ds.getConnection();
            XQPreparedExpression exp = con.prepareExpression(is);
            exp.bindString(new QName("tipo"),type,null);
            XQResultSequence rs = exp.executeQuery();
            while (rs.next()) {
                elem = (Element)rs.getNode();
                id = elem.getAttribute("id");
                nombre = elem.getAttribute("nombre");
                numAtaques = Integer.parseInt(elem.getAttribute("num-ataques"));
                if (id!= null){
                    abedemons.add(new Abedemon(id,nombre,numAtaques));
                }
            }
            rs.close();
            exp.close();
            con.close();
        } catch (XQException e) {
            e.printStackTrace();
        }
        return abedemons;
    }

    /**
     * Obtiene la descripción de una especie de abedemon.
     * 
     * @param id ID de la especie a describir
     * @return Código XHTML con la descripción de la especie
     */
    @Override
    public String getAbedemonDescription(String id) {
        InputStream is = getClass().getResourceAsStream("Abedemon3.xquery");
        XQConnection con;
        String result = null;
        try {
            con = ds.getConnection();
            XQPreparedExpression exp = con.prepareExpression(is);
            exp.bindString(new QName("id"),id,null);
            XQResultSequence rs = exp.executeQuery();
            while (rs.next()) {
                result = rs.getItemAsString(null);
            }
            rs.close();
            exp.close();
            con.close();
        } catch (XQException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Obtiene el daño máximo recibido por un abedemon de una especie dada al
     * ser atacado por otro.
     * 
     * @param idAttacker ID de la especie del atacante.
     * @param idReceiver ID de la especie que recibe el daño.
     * @return Máximo daño que puede infligir el atacante.
     */
    @Override
    public Integer getDamage(String idAttacker, String idReceiver) {
        InputStream is = getClass().getResourceAsStream("Abedemon4.xquery");
        XQConnection con;
        int damage = 0;
        try {
            con = ds.getConnection();
            XQPreparedExpression exp = con.prepareExpression(is);
            exp.bindString(new QName("yoId"),idAttacker,null);
            exp.bindString(new QName("adversarioId"),idReceiver,null);
            XQResultSequence rs = exp.executeQuery();
            while (rs.next()) {
                damage = Integer.parseInt(rs.getItemAsString(null));
            }
            rs.close();
            exp.close();
            con.close();
        } catch (XQException e) {
            e.printStackTrace();
        }
        return damage;
    }
}
