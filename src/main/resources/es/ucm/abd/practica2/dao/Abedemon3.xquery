xquery version "3.0";

declare variable $id as xs:string external;

for $especie in doc("Abedemon.xml")/bd-abedemon//especie[@id = $id]
return
    <html>
        <head></head>
        <body>
            <h1> {data($especie/nombre)} </h1>
            <img src="{data($especie/url)}"/>
            <p> {data($especie/descripcion)} </p>
            <p><b>Tipos: </b>
                {string-join($especie/tipo,',')}
            </p>
            <p><b>Ataques: </b>
                {
                    string-join(for $ataqueid in $especie/ataques/tiene-ataque/@id
                    let $ataque:= doc("Abedemon.xml")/bd-abedemon/ataque[@id = $ataqueid]
                    order by $ataque/nombre
                    return $ataque/nombre,',')
                }
            </p>
            <p><b>Evoluciones: </b>
                <ul>
                    {for $evolucion in $especie//evoluciones/especie
                    return <li>
                        <a href="{data($evolucion/@id)}"> {data($evolucion/nombre)}
                        </a></li>
                    }
                </ul>
            </p>
        </body>
    </html>