xquery version "3.0";

declare variable $tipo as xs:string external;

for $especie in doc("Abedemon.xml")/bd-abedemon//especie
where $especie/tipo = $tipo
return <resultado id="{data($especie/@id)}" nombre="{data($especie/nombre)}"
        num-ataques="{count(($especie/ataques/tiene-ataque))}"/>
