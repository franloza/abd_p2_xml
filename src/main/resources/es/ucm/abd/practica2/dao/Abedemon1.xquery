xquery version "3.0";

distinct-values(for $tipo in doc("Abedemon.xml")/bd-abedemon//especie/tipo
order by $tipo
return data($tipo))