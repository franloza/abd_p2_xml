xquery version "3.0";

declare namespace functx = "http://www.functx.com";
declare function functx:if-empty
( $arg as item()? ,
        $value as item()* )  as item()* {

    if (string($arg) != '')
    then data($arg)
    else $value
} ;

declare variable $yoId as xs:string external;
declare variable $adversarioId as xs:string external;

let $tiposAdversario := (for $tipo in doc("Abedemon.xml")/bd-abedemon//
        especie[@id=$adversarioId]/tipo
return data($tipo))

let $idsAtaques :=
    (for $ataques in doc("Abedemon.xml")/bd-abedemon//especie[@id=$yoId]/ataques/
            tiene-ataque
    return $ataques)

let $ataques := (
    for $ataque in $idsAtaques
    return doc("Abedemon.xml")/bd-abedemon/ataque[@id=$ataque/@id]
)

let $maxDaño := max(
        for $ataque in $ataques
        return
            for $tipo in $tiposAdversario
            return data($ataque/daño[@tipo = $tipo]/@valor)
)

return functx:if-empty($maxDaño,0)
